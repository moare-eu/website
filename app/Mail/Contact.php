<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The mail adresse of contact.
     *
     * @var string
     */
    public $email;

    /**
     * The content of contact message.
     *
     * @var string
     */
    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $msg)
    {   
        $this->email = $email;
        $this->msg = $msg;
        $this->subject = "[MOARE] Contact message over website";
        $this->replyTo([$email]);
        $this->to(["info@moare.eu"]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
                    ->view('emails.contact')
                    ->with('msg', $this->msg)
                    ->with('email', $this->email);
    }
}
