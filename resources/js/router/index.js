import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home'
import Privacy from '../components/Privacy'

Vue.use(VueRouter)
export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/privacy',
      name: 'Privacy',
      component: Privacy
    },
  ],
  mode: 'history'
})
