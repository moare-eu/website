import App from './App.vue'
import 'bootstrap'
import router from './router'

window.Vue = require('vue');

require('./bootstrap');

Vue.config.productionTip = false

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
